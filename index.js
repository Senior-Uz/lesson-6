// Selectors

let numberOfButtons = document.querySelectorAll(".buttonColor").length;
let changing = document.querySelector("body");

// Adding event listener
for (let i = 0; i < numberOfButtons; i++) {
  document
    .querySelectorAll(".buttonColor")
    [i].addEventListener("click", function () {
      let buttonInnerHTML = this.innerHTML;

      switch (buttonInnerHTML) {
        case "1":
          changing.style.background = "lightskyblue";

          break;
        case "2":
          changing.style.background = "yellow";

          break;
        case "3":
          changing.style.background = "red";

          break;
        case "4":
          changing.style.background = "blue";

          break;
        case "5":
          changing.style.background = "greenyellow";

          break;
        case "6":
          changing.style.background = "purple";

          break;
        case "7":
          changing.style.background = "black";
        default:
          break;
      }
    });
}
